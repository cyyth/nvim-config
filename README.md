# My Neovim IDE
This is my Neovim [0.9] configuration, which I tested on Fedora Workstation 37 and Windows 11. It is currently not polishted AT ALL, but quite readable. Please do not use it.

## But if you insist

### Fedora 37 Workstation
Should works out of the box. Clone and rename this repo. into ~/.config/nvim.
Other frequently updated Linux distrobution should work as well.

### Windows
Download the latest Neovim [0.9]. Then clone this repo. and rename this repository into C:\Users\<USER>\AppData\Local\nvim. After installing all the dependencies, Lazy should install all the plugin automagically.

- You probably need to install gcc from (https://www.msys2.org/);
- and npm as well (https://nodejs.org/en/download);
- and ofcourse Python 3 and Git if you do not have those already.

These are needed by the LSP.

Lastly for any operating system, get you favorite Nerd Font from https://www.nerdfonts.com/font-downloads to make icons and glyps show correctly.

### LaTeX Capability
- Install Zathura with MuPdf;
- Install LaTeX packages

![alt text](asset/editor.png)
