-- [Lazy bootstrapping]
local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		'git',
		'clone',
		'--filter=blob:none',
		'https://github.com/folke/lazy.nvim.git',
		'--branch=stable', -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

-- [General vim configs]
-- Editor behaviour
local indent = 4
vim.opt.tabstop = indent
vim.opt.shiftwidth = indent
vim.opt.softtabstop = indent
vim.opt.smarttab = true
vim.opt.encoding = 'utf8'
vim.opt.wrapscan = true
vim.opt.writebackup = false
vim.opt.clipboard = 'unnamedplus' -- universal clipboard

-- Split panes management
vim.opt.splitright = true -- split the screen on the right
vim.opt.splitbelow = true -- split the screen on the botton

-- Editor Appearance
vim.opt.cmdheight = 1           -- cmd line height
vim.opt.colorcolumn = { '100' } -- characters limit indicator
vim.opt.fillchars = 'eob: '     -- remove tilde at the eob
vim.opt.listchars:append({ tab = '│ ' })
vim.opt.listchars:append({ trail = '•' })
vim.opt.listchars:append({ eol = '↲' })
vim.opt.listchars:append({ nbsp = '␣' })
vim.opt.listchars:append({ extends = '›' })
vim.opt.listchars:append({ precedes = '‹' })
vim.opt.list = true
vim.opt.showbreak = '↳ '
vim.opt.smartcase = true
vim.opt.showmode = false -- no mode
vim.opt.termguicolors = true
vim.opt.wrap = false
vim.opt.textwidth = 80
vim.wo.cursorline = true

-- Ruler and number configs
vim.opt.ruler = true
vim.opt.number = true -- show line number
vim.opt.relativenumber = true

vim.cmd [[
	" Italics fix
    set t_ZH=^[[3m
    set t_ZR=^[[23m

	" Highlight yank
	augroup highlight_yank
		autocmd!
		au TextYankPost * silent! lua vim.highlight.on_yank { higroup='IncSearch', timeout=600 }
	augroup END

	" Set wrap on .tex file
	augroup WrapLineInTextFile
		autocmd!
		autocmd FileType tex setlocal wrap
		autocmd FileType markdown setlocal wrap
	augroup END
	]]

-- [Set the leader key before loading lazy]
require('remap')   -- General reamapping
require('plugins') -- Plugins manager

-- [When using nvim in vscode]
require('configs.comment')
require('configs.which-key')
require('nvim-surround').setup()
require('leap').setup({ case_sensitive = false })

require('treesj').setup({
	use_default_keymaps = false,
	cursor_behavior = 'hold',
	notify = true,
})

-- [These only load when we are not in vscode]
if not vim.g.vscode then
	require('configs.telescope')
	require('configs.treesitter')
	require('configs.lsp')
	require('configs.tex')

	-- UI plugins
	require('configs.colorscheme')
	require('configs.gitsigns')
	require('configs.lualine')
	require('configs.bar')
	require('configs.tree')
	require('configs.aerial')
	require('configs.notify')

	-- Small Plugins
	require('colorizer').setup()
	require('virt-column').setup({ char = '│' })        --set the virtual column character
	vim.api.nvim_set_hl(0, 'VirtColumn', { fg = '#28282f' }) -- set the virtual column color
end
