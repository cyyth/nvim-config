require('aerial').setup({
	backends = { 'treesitter', 'lsp' },
	filter_kind = false,
	show_guides = true,
	guides = {
		-- When the child item has a sibling below it
		mid_item = '├─',
		-- When the child item is the last in the list
		last_item = '└─',
		-- When there are nested child guides to the right
		nested_top = '│ ',
		-- Raw indentation
		whitespace = '  ',
	},
	-- Options for the floating nav windows
	nav = {
		border = 'single',
		-- max_height = 0.9,
		-- min_height = { 10, 0.1 },
		-- max_width = 0.5,
		-- min_width = { 0.2, 20 },
		autojump = true,
		preview = false,
		keymaps = {
			['<CR>'] = 'actions.jump',
			['<2-LeftMouse>'] = 'actions.jump',
			['|'] = 'actions.jump_vsplit',
			['-'] = 'actions.jump_split',
			['h'] = 'actions.left',
			['l'] = 'actions.right',
			['<esc>'] = 'actions.close',
		},
	},

	lsp = {
		diagnostics_trigger_update = true,
		update_when_errors = true,
		update_delay = 300,
	},
	on_attach = function(bufnr)
		-- Jump forwards/backwards with '{' and '}'
		vim.keymap.set('n', 'm', '<cmd>AerialPrev<CR>', { buffer = bufnr })
		vim.keymap.set('n', ',', '<cmd>AerialNext<CR>', { buffer = bufnr })
	end
})
