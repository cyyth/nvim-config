vim.g.barbar_auto_setup = false -- disable auto-setup
require 'barbar'.setup {
	animation = true,
	auto_hide = false,
	tabpages = true,
	clickable = true,
	focus_on_close = 'left',
	highlight_alternate = false,
	highlight_inactive_file_icons = false,
	highlight_visible = true,
	icons = {
		buffer_index = false,
		buffer_number = false,
		button = '',
		diagnostics = {
			[vim.diagnostic.severity.ERROR] = { enabled = false },
			[vim.diagnostic.severity.WARN] = { enabled = false },
			[vim.diagnostic.severity.INFO] = { enabled = false },
			[vim.diagnostic.severity.HINT] = { enabled = false },
		},
		gitsigns = {
			added = { enabled = false, icon = '' },
			changed = { enabled = false, icon = '' },
			deleted = { enabled = false, icon = '' },
		},
		filetype = {
			custom_colors = false,
			enabled = true,
		},
		separator = { left = '', right = '' },
		modified = { button = '●' },
		pinned = { button = '車', filename = true, separator = { right = '' } },
		alternate = { filetype = { enabled = false } },
		current = { buffer_index = false },
		inactive = { button = '×' },
		visible = { modified = { buffer_number = false } },
	},
	insert_at_end = false,
	insert_at_start = false,
	maximum_padding = 1,
	minimum_padding = 1,
	maximum_length = 30,
	semantic_letters = true,
	sidebar_filetypes = {
		NvimTree = true
	},
	letters = 'asdfjkl;ghnmxcvbziowerutyqpASDFJKLGHNMXCVBZIOWERUTYQP',
	no_name_title = nil,
}
