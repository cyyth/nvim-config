-- setup web-devicons
require('nvim-web-devicons').setup({
	color_icons = true,
})

require('onedark').setup {
	style = 'dark',
	transparent = true,
	term_colors = true,
	code_style = {
		comment = 'italic',
	},
	diagnostics = {
		undercurl = true,
		background = false,
	}
}

require('nightfox').setup({
	options = {
		transparent = true,
		terminal_colors = true,
		styles = {
			comments = 'italic',
			keywords = 'bold,italic',
			types = 'bold'
		},
		inverse = {
			match_paren = false,
		},
	},
	palettes = {
		all = {
			bg0 = '#17171e',
			fg3 = '#373a44',
			bg3 = '#23292f',
		}
	},
})

vim.cmd('colorscheme nordfox')
-- require('onedark').load()
