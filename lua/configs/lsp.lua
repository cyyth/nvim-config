local lsp = require('lsp-zero')
local border_style = 'single'

lsp.preset({
	name = 'recommended',
	suggest_lsp_servers = true,
	float_boarder = border_style,
})

lsp.ensure_installed({
	'tsserver',
	'eslint',
	'lua_ls',     -- lua
	'rust_analyzer', --rust
	'bashls',
	'clangd',     -- cpp and c
	'cmake',      --cmake
	'julials',    --julia
	'marksman',   -- markdown
	'texlab',     -- latex
	'pylsp',      -- python
	'lemminx',    --xml
	'yamlls',     --YAML
	'jsonls',     --json
	'html',       --html
	'docker_compose_language_service',
	'dockerls',
})

lsp.set_sign_icons({
	-- hide all the sign icons
	error = '',
	warn = '',
	hint = '',
	info = ''
})

lsp.setup()

vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with(
	vim.lsp.handlers.signature_help, {
		border = 'single'
	}
)

vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(
	vim.lsp.handlers.hover, {
		border = border_style,
	}
)

local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()

cmp.setup({
	window = {
		completion = cmp.config.window.bordered({
			border = border_style,
			winhighlight = '',
		}),
		documentation = cmp.config.window.bordered({
			border = border_style,
			winhighlight = '',
		})
	},
	mapping = {
		['<Tab>'] = cmp_action.tab_complete(),
		['<S-Tab>'] = cmp_action.select_prev_or_fallback(),
	},
	formatting = {
		fields = { 'abbr', 'kind', 'menu' },
		format = require('lspkind').cmp_format({
			mode = 'symbol_text',
			maxwidth = 40,
			ellipsis_char = '...',
		})
	},
	completion = {
		keyword_length = 4,
	},
})
