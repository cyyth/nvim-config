local colors = {
	darkwhite = '#16161d',
	gray = '#eeeeee',
	dark_gray = '#535375',
	white = '#eeeeee',
	-- bg = nil,
	bg = '#17171e',
	normal = '#7e9cd8',
	insert = '#98bb6c',
	visual = '#a9a1e1',
	replace = '#e46876',
	command = '#e6c384',
	yellow = '#ECBE7B',
	cyan = '#008080',
	darkblue = '#081633',
	green = '#98be65',
	orange = '#FF8800',
	violet = '#a9a1e1',
	magenta = '#c678dd',
	blue = '#51afef',
	red = '#ec5f67'
}

local theme = function()
	return {
		inactive = {
			a = { fg = colors.dark_gray, bg = colors.bg, gui = 'bold' },
			b = { fg = colors.dark_gray, bg = colors.bg },
			c = { fg = colors.dark_gray, bg = colors.bg },
		},
		visual = {
			a = { fg = colors.visual, bg = colors.bg, gui = 'bold' },
			b = { fg = colors.white, bg = colors.bg },
			c = { fg = colors.white, bg = colors.bg },
		},
		replace = {
			a = { fg = colors.replace, bg = colors.bg, gui = 'bold' },
			b = { fg = colors.white, bg = colors.bg },
			c = { fg = colors.white, bg = colors.bg },
		},
		normal = {
			a = { fg = colors.normal, bg = colors.bg, gui = 'bold' },
			b = { fg = colors.white, bg = colors.bg },
			c = { fg = colors.white, bg = colors.bg },
		},
		insert = {
			a = { fg = colors.insert, bg = colors.bg, gui = 'bold' },
			b = { fg = colors.white, bg = colors.bg },
			c = { fg = colors.white, bg = colors.bg },
		},
		command = {
			a = { fg = colors.command, bg = colors.bg, gui = 'bold' },
			b = { fg = colors.white, bg = colors.bg },
			c = { fg = colors.white, bg = colors.bg },
		},
	}
end

local my_filename = {
	'filename',
	file_status = true, -- Displays file status (readonly status, modified status)
	newfile_status = false, -- Display new file status (new file means no write after created)
	path = 1,           -- 0: Just the filename
	-- 1: Relative path
	-- 2: Absolute path
	-- 3: Absolute path, with tilde as the home directory
	-- 4: Filename and parent dir, with tilde as the home directory

	shorting_target = 40, -- Shortens path to leave 40 spaces in the window
	-- for other components. (terrible name, any suggestions?)
	symbols = {
		modified = '', -- Text to show when the file is modified.
		readonly = '', -- Text to show when the file is non-modifiable or readonly.
		unnamed = '[No Name]', -- Text to show for unnamed buffers.
		newfile = '[New]', -- Text to show for newly created file before first write
	}
}

local my_lsp_progress = {
	'lsp_progress',
	colors = {
		percentage      = colors.white,
		title           = colors.white,
		message         = colors.white,
		spinner         = colors.green,
		lsp_client_name = colors.green,
		use             = true,
	},
	separators = {
		component = ' ',
		progress = ' | ',
		percentage = { pre = '', post = '%% ' },
		title = { pre = '', post = ': ' },
		lsp_client_name = { pre = '', post = ' ' },
		spinner = { pre = '', post = '' },
		message = { commenced = 'In Progress', completed = 'Completed' },
	},
	display_components = { 'spinner', 'lsp_client_name', { 'title', 'percentage', 'message' } },
	timer = { progress_enddelay = 6000, spinner = 6000, lsp_client_name_enddelay = 6000 },
	spinner_symbols = { '🌑', '🌒', '🌓', '🌔', '🌕', '🌖', '🌗', '🌘' },
}

local config = {
	options = {
		icons_enabled = true,
		theme = theme(),
		component_separators = { left = '', right = '' },
		section_separators = { left = '', right = '' },
		disabled_filetypes = {
			'packer',
			'NvimTree',
			'aerial'
		},
		ignore_focus = {},
		always_divide_middle = true,
		globalstatus = false,
		refresh = {
			statusline = 1000,
			tabline = 1000,
			winbar = 1000,
		}
	},
	sections = {
		lualine_a = { 'mode' },
		lualine_b = {
			my_filename,
			{
				'branch',
				icon = '',
				color = {
					fg = '#e6c384'
				}
			},
			{
				'diff',
				colored = true,
				symbols = { added = '󰐙 ', modified = '󰝶 ', removed = '󰍷 ' },
			},
			{}
		},
		lualine_c = {},
		lualine_x = {
			my_lsp_progress,
		},
		lualine_y = {
			'diagnostics',
			'progress',
		},
		lualine_z = { 'location' }
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = { my_filename },
		lualine_x = { 'location' },
		lualine_y = {},
		lualine_z = {}
	},
	tabline = {},
	winbar = {
		lualine_a = {},
		lualine_b = {
			{
				'aerial',
				sep = ' 󰅂 '
			},
		},
		lualine_y = {},
		lualine_z = {},
	},
	inactive_winbar = {
		lualine_b = {
			{
				'aerial',
				sep = ' 󰅂 '
			},
		},
		lualine_y = {},
	},
	extensions = {}
}

require('lualine').setup(config)
