require('notify').setup({
	background_colour = '#000000',
	timeout = 2000,
	fps = 30,
	render = 'compact',
	stages = 'slide',
})

vim.notify = require('notify')
