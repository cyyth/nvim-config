vim.cmd [[
	let g:vimtex_view_method = 'zathura' " Viewer selection

	" LaTeX complier
	let g:vimtex_compiler_latexmk = {
		\ 'executable' : 'latexmk',
		\ 'options' : [
		\   '-xelatex',
		\   '-file-line-error',
		\   '-synctex=1',
		\   '-interaction=nonstopmode',
		\ ],
		\}

	let maplocalleader = " "

	hi! link texMathEnvArgName texEnvArgName
	hi! link texMathZone LocalIdent
	hi! link texMathZoneEnv texMathZone
	hi! link texMathZoneEnvStarred texMathZone
	hi! link texMathZoneX texMathZone
	hi! link texMathZoneXX texMathZone
	hi! link texMathZoneEnsured texMathZone

	" Small tweaks
	hi! link QuickFixLine Normal
	hi! link qfLineNr Normal
	hi! link EndOfBuffer LineNr
	hi! link Conceal LocalIdent

	function! SyncTexForward()
		let linenumber=line(".")
		let colnumber=col(".")
		let filename=bufname("%")
		let filenamePDF=filename[:-4]."pdf"
		let execstr="!zathura --synctex-forward " . linenumber . ":" . colnumber . ":" . filename . " " . filenamePDF . "&>/dev/null &"
		silent exec execstr
	endfunction

	nnoremap <silent> <localleader>lf :call<space>SyncTexForward()<cr>
]]
