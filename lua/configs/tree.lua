-- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- setup with some options
require('nvim-tree').setup({
	sort_by = 'case_sensitive',
	view = {
		width = 28,
	},
	renderer = {
		root_folder_modifier = ':~',
		indent_markers = {
			enable = true,
		},
		icons = {
			glyphs = {
				default = '󰈙',
				symlink = '󱅷',
				folder = {
					arrow_open = '󰅀',
					arrow_closed = '󰅂',
					default = '󰉋',
					open = '󰝰',
					empty = '󰉖',
					empty_open = '󰷏',
					symlink = '󰉒',
					symlink_open = '󰉒',
				},
				git = {
					unstaged = '󰰨',
					staged = '󰰢',
					unmerged = '',
					renamed = '󰣕',
					untracked = '',
					deleted = '󰍷',
					ignored = '󱨧',
				},
			},
		},
	},
	diagnostics = {
		enable = true,
		show_on_dirs = false,
		icons = {
			hint = '󰌶',
			info = '',
			warning = '',
			error = '',
		},
	},
	filters = {
		dotfiles = false,
	},
})
