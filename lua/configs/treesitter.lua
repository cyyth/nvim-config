require('nvim-treesitter.configs').setup {
	ensure_installed = {
		'c',
		'cpp',
		'lua',
		'julia',
		'vim',
		'vimdoc',
		'rust',
		'python',
		'html',
		'latex',
		'bibtex',
		'markdown',
		'matlab',
		'yaml',
		'json',
		'cmake',
		'bash',
		'dockerfile'
	},

	sync_install = false,
	auto_install = false,
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
	},
}
