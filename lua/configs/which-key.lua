local wk = require('which-key')

wk.setup({
	plugins = {
		presets = {
		},
	}
})

-- User Interface
wk.register({
	['<leader>'] = { '<cmd>Telescope find_files theme=ivy<cr>', '[telescope] Find files' },
	a = {
		name = 'aerial',
		{ '<cmd>AerialToggle!<CR>', '[aerial] Toggle symbols pane' },
		n = { '<cmd>AerialNavOpen<CR>', '[aerial] Open AerialNav' },
	},
	e = { '<cmd>NvimTreeToggle<cr>', '[nvim-tree]Toggle the side file explorer' },
	['-'] = { '<cmd>new<cr>', 'Split horizontal' },
	['|'] = { '<cmd>vnew<cr>', 'Split horizontal' },
	t = { '<cmd>enew<cr>', 'New buffer' },
}, { prefix = '<leader>' })

-- Buffer navigaitons
wk.register({
	['<TAB>'] = { '<cmd>BufferNext<cr>', '[barbar] Move to the next buffer' },
	['<S-TAB>'] = { '<cmd>BufferPrevious<cr>', '[barbar] Move to the previous buffer' },
	['<C-p>'] = { '<cmd>BufferPin<cr>', '[barbar] Pin current buffer' },
	['<C-c>'] = { '<cmd>BufferClose<cr>', '[barbar] Close current buffer' },
})

-- Debugging
wk.register({
	c = {
		name = 'quick fix',
		o = { '<cmd>copen<cr>', 'Open quick fix' },
		c = { '<cmd>cclose<cr>', 'Close quick fix' },
	}
}, { prefix = '<leader>' })

-- Vimtex (LaTeX support)
wk.register({
	l = {
		name = 'vimtex',
		l = '[vimtex] Compile',
		f = '[vimtex] Forward searching',
		i = '[vimtex] Show information',
		I = '[vimtex] Show all information',
		g = '[vimtex] Show status',
		G = '[vimtex] Show all status',
		a = '[vimtex] Context menu',
		c = '[vimtex] Clean',
		C = '[vimtex] Clean full',
		e = '[vimtex] Show error',
		k = '[vimtex] Stop',
		K = '[vimtex] Stop all',
		L = '[vimtex] Compile selected',
		m = '[vimtex] Show imaps',
		o = '[vimtex] Compile output',
		q = '[vimtex] Show log',
		s = '[vimtex] Toggle main',
		t = '[vimtex] Open table of contents',
		T = '[vimtex] Toggle table of contents',
		v = '[vimtex] View the compiled file',
		x = '[vimtex] Reload',
		X = '[vimtex] Reload state',
	}
}, { prefix = '<space>' })

-- Leap
wk.register({
	name = 'leap',
	f = { '<Plug>(leap-forward-to)', '[leap] forward' },
	b = { '<Plug>(leap-backward-to)', '[leap] backward' },
}, { prefix = '<space>' })

-- Panes navigaitons
wk.register({
	name = 'pane navigation',
	['<C-h>'] = { 'Move to the left split pane' },
	['<C-j>'] = { 'Move to the below split pane' },
	['<C-k>'] = { 'Move to the above split pane' },
	['<C-l>'] = { 'Move to the right split pane' },
	['<C-A-h>'] = { '<cmd>vertical resize +2<cr>', 'Resize the split pane ' },
	['<C-A-j>'] = { '<cmd>resize -2<cr>', 'Resize the split pane ' },
	['<C-A-k>'] = { '<cmd>resize +2<cr>', 'Resize the split pane ' },
	['<C-A-l>'] = { '<cmd>vertical resize -2<cr>', 'Resize the split pane ' },
})

-- LSP
wk.register({
	name = 'LSP',
	K = '[LSP] Show hover',
	g = {
		d = '[LSP] Go to the definition',
		D = '[LSP] Go to the declaration',
		o = '[LSP] Show type definition',
		r = '[LSP] Show all the references',
		s = '[LSP] Show the signature help',
	},
	['<F2>'] = '[LSP] Buffer rename',
	['<F3>'] = '[LSP] Format the buffer',
	['<F4>'] = '[LSP] Buffer code action'
})

-- Extras
wk.register({
	[','] = '[aerial] Next symbols',
	m = '[aerial] Previous symbols',
	['<leader><Tab>'] = { '<cmd>TSJToggle<cr>', '[treesj] Toggle splitting/joining current code block' }
})
