return require('lazy').setup({
	-- Colorscheme
	'nvim-tree/nvim-web-devicons', -- glyps support for many plugins
	{ 'navarasu/onedark.nvim',  lazy = true },
	{ 'EdenEast/nightfox.nvim', lazy = false },

	-- Git integration
	'lewis6991/gitsigns.nvim', -- git status on the gutter

	-- User interface
	'nvim-lualine/lualine.nvim',   -- status line
	'arkav/lualine-lsp-progress',  -- show the lsp progress on lualine
	'romgrk/barbar.nvim',          -- buffer bar
	'nvim-tree/nvim-tree.lua',     -- file explorer
	'folke/which-key.nvim',        -- remapping hotkeys and naming them
	'lukas-reineke/virt-column.nvim', -- char limit line
	'rcarriga/nvim-notify',        -- fancy notification
	'stevearc/aerial.nvim',        -- symbols tree and navigation

	-- Quality of life
	{
		'nvim-telescope/telescope.nvim',
		branch = '0.1.x',
		dependencies = { { 'nvim-lua/plenary.nvim' } }
	},                                                   -- fuzzy finder
	'norcalli/nvim-colorizer.lua',                       -- show hex color on the editor
	'numToStr/Comment.nvim',                             -- useful commenter
	'ggandor/leap.nvim',                                 -- better jumping (leaping)
	{
		'kylechui/nvim-surround',
		dependencies = { 'nvim-treesitter/nvim-treesitter' }
	},                                                   -- easy tool to edit surronding symbols
	'Wansmer/treesj',                                    -- easily change from one-line to multi-line

	-- Extra languages support
	'JuliaEditorSupport/julia-vim', -- supports latex to unicode

	-- LaTeX editor
	'lervag/vimtex', -- filetype and syntacx plugin for LaTeX

	-- LSP, completion and Treesitter
	'onsails/lspkind.nvim', -- lsp snippet glyps
	{
		'nvim-treesitter/nvim-treesitter',
		build = function()
			pcall(require('nvim-treesitter.install').update { with_sync = true })
		end,
	},
	{
		'VonHeikemen/lsp-zero.nvim',
		branch = 'v2.x',
		dependencies = {
			-- LSP Support
			{ 'neovim/nvim-lspconfig' }, -- Required
			{
				-- Optional
				'williamboman/mason.nvim',
				build = function()
					pcall(vim.cmd, 'MasonUpdate')
				end,
			},
			{ 'williamboman/mason-lspconfig.nvim' }, -- Optional
			-- Autocompletion
			{ 'hrsh7th/nvim-cmp' },         -- Required
			{ 'hrsh7th/cmp-nvim-lsp' },     -- Required
			{ 'hrsh7th/cmp-path' },         -- Optional
			{ 'hrsh7th/cmp-nvim-lua' },     -- Optional
			{ 'L3MON4D3/LuaSnip' },         -- Required
		}                                   -- All lsp functions
	},
})
