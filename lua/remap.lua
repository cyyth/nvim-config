-- change the leader key to '\'
vim.g.mapleader = '\\'
vim.g.maplocalleader = '\\'

-- move between splits
vim.keymap.set('n', '<C-h>', '<C-w>h')
vim.keymap.set('n', '<C-j>', '<C-w>j')
vim.keymap.set('n', '<C-k>', '<C-w>k')
vim.keymap.set('n', '<C-l>', '<C-w>l')
